package com.fabrica.moscow.services.iam;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HolaMundo {

	@RequestMapping("/")
    public String index() {
        return "Hola Mundo";
    }

}
